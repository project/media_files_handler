<?php

namespace Drupal\media_files_handler\Service;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\file\Entity\File;
use Drupal\file\FileRepository;
use Drupal\file\FileUsage\FileUsageInterface;
use Drupal\media\MediaInterface;
use Psr\Log\LoggerInterface;

/**
 * Provide Tools to find files to delete.
 */
class HandlerService {

  /**
   * The default delete action.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   *
   * @todo Expose as Configuration.
   */
  private $deleteAction = 'temporary';

  /**
   * Drupal file_system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The media storage.
   *
   * @var \Drupal\Core\Entity\ContentEntityStorageInterface
   */
  protected $mediaStorage;

  /**
   * The file storage.
   *
   * @var \Drupal\Core\Entity\ContentEntityStorageInterface
   */
  protected $fileStorage;

  /**
   * The Media Files Handler logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The file usage service.
   *
   * @var \Drupal\file\FileUsage\FileUsageInterface
   */
  protected $fileUsage;

  /**
   * The file repository service.
   *
   * @var \Drupal\file\FileRepository
   */
  protected $fileRepository;

  /**
   * Constructs a new HandlerService object.
   *
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   The Media Files Handler logger.
   * @param \Drupal\file\FileUsage\FileUsageInterface $fileUsage
   *   The file usage service.
   * @param \Drupal\file\FileRepository $fileRepository
   *   The file repository service.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(FileSystemInterface $file_system, EntityTypeManagerInterface $entity_type_manager, LoggerInterface $logger, FileUsageInterface $fileUsage, FileRepository $fileRepository) {
    $this->fileSystem = $file_system;
    $this->entityTypeManager = $entity_type_manager;
    $this->mediaStorage = $entity_type_manager->getStorage('media');
    $this->fileStorage = $this->entityTypeManager->getStorage('file');
    $this->logger = $logger;
    $this->fileUsage = $fileUsage;
    $this->fileRepository = $fileRepository;
  }

  /**
   * Loop through all fields to find those which reference a file entity.
   *
   * @param \Drupal\media\MediaInterface $entity
   *   The current entity.
   *
   * @return array
   *   Array of field names.
   */
  private function getAllFieldsWithFiles(MediaInterface $entity): array {
    $fields = [];

    // Find all fields with files.
    $field_definitions = $entity->getFieldDefinitions();
    foreach ($field_definitions as $field_definition) {
      $type = $field_definition->getType();
      if ($type === 'image' || $type === 'file') {
        $fields[$field_definition->getName()] = $field_definition;
      }
    }
    return $fields;
  }

  /**
   * Find every file used of a media entity revision.
   *
   * This can be many files depending on usage of
   * translations and revisions. The default revision is not included!
   *
   * @param \Drupal\media\MediaInterface $entity
   *   The current media entity.
   * @param array $excluded_languages
   *   Languages to skip deletion.
   *
   * @return array
   *   All files used by the entity.
   */
  private function getAllFilesFromDefaultRevisionFromEntity(MediaInterface $entity, array $excluded_languages = []): array {
    $fields = $this->getAllFieldsWithFiles($entity);
    return $this->getAllFilesPerLanguage($entity, $excluded_languages);
  }

  /**
   * Find every file used of a media entity revision.
   *
   * This can be many files depending on usage of
   * translations and revisions. The default revision is not included!
   *
   * @param \Drupal\media\MediaInterface $entity
   *   The current media entity.
   * @param array $excluded_languages
   *   Languages to skip deletion.
   *
   * @return array
   *   All files used by the entity.
   */
  private function getAllFilesFromAllRevisionsFromEntity(MediaInterface $entity, array $excluded_languages = []): array {
    $files = [];

    // Get all revisions to not overlook an old file.
    $entity_revisions = $this->mediaStorage->getQuery()
      ->accessCheck(FALSE)
      ->allRevisions()
      ->condition($entity->getEntityType()->getKey('id'), $entity->id())
      ->execute();
    $entity_revisions = $this->mediaStorage->loadMultipleRevisions(array_keys($entity_revisions));

    foreach ($entity_revisions as $revision) {
      array_push($files, ...$this->getAllFilesPerLanguage($revision, $excluded_languages));
    }

    return $files;
  }

  /**
   * Convert a uri from private to public.
   *
   * Used as target to file movement.
   *
   * @param string $uri
   *   The current uri.
   *
   * @return string
   *   The new uri.
   */
  private function changeFilePathToPrivate(string $uri): string {
    if (strpos($uri, 'private://') === FALSE) {
      $uri = str_replace('public://', 'private://', $uri);
    }
    return $uri;
  }

  /**
   * Convert a uri from public to private.
   *
   * Used as target to file movement.
   *
   * @param string $uri
   *   The current uri.
   *
   * @return string
   *   The new uri.
   */
  private function changeFilePathToPublic(string $uri): string {
    if (strpos($uri, 'public://') === FALSE) {
      $uri = str_replace('private://', 'public://', $uri);
    }
    return $uri;
  }

  /**
   * Delete or mark file as temporary.
   *
   * @param \Drupal\file\Entity\File $file
   *   The file.
   * @param \Drupal\media\MediaInterface $entity
   *   The current media entity.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function deleteFile(File $file, MediaInterface $entity): void {
    $uri = $file->getFileUri();

    // Ignore generic thumbnails.
    if (strpos($uri, 'generic/generic.png') !== FALSE) {
      return;
    }

    if ($this->deleteAction === 'delete') {
      $file->delete();
      $this->logger->info(
        'Deleted file "@fileName" (@filedId) of Media Entity @mediaId.',
        [
          '@fileName' => $file->getFilename(),
          '@filedId' => $file->id(),
          '@mediaId' => $entity->id(),
        ],
      );
    }
    elseif ($this->deleteAction === 'temporary') {
      // Remove all file usages for current file,
      // otherwise cron will not delete the file.
      $file_usage = $this->fileUsage->listUsage($file);
      if (!empty($file_usage)) {
        foreach ($file_usage as $module => $usage) {
          // Specify count as high value to make sure usage
          // count is not just decremented by 1.
          // Using translations and revisions has many errors
          // regarding file usage count, it is mostly way too high.
          $this->fileUsage->delete($file, $module, NULL, NULL, 1000);
        }
      }

      $file->setTemporary();
      $file->save();

      $this->logger->info(
        'Marked file "@fileName" (@filedId) of Media Entity @mediaId as temporary.',
        [
          '@fileName' => $file->getFilename(),
          '@filedId' => $file->id(),
          '@mediaId' => $entity->id(),
        ],
      );
    }
  }

  /**
   * Loop through all languages of the current entity to find all used files.
   *
   * @param \Drupal\media\MediaInterface $entity
   *   The current entity.
   * @param array $excluded_languages
   *   Languages to skip deletion.
   *
   * @return array
   *   All files found.
   */
  private function getAllFilesPerLanguage(MediaInterface $entity, array $excluded_languages = []): array {
    $files = [];
    $fields = $this->getAllFieldsWithFiles($entity);

    // Loop through all languages to not miss translated files.
    $languages = $entity->getTranslationLanguages(TRUE);
    foreach ($languages as $language) {
      $langcode = $language->getId();
      if (!in_array($langcode, $excluded_languages) && $entity->hasTranslation($langcode)) {
        $entity = $entity->getTranslation($langcode);

        foreach ($fields as $field_name => $field) {
          if ($entity->hasField($field_name) && !$entity->get($field_name)->isEmpty()) {
            $values = $entity->get($field_name)->getValue();
            foreach ($values as $value) {
              if ($value['target_id'] !== NULL) {
                $files[$value['target_id']] = $value['target_id'];
              }
            }
          }
        }
      }
    }

    return $files;
  }

  /**
   * Moves a file to private storage, so an access check can be performed.
   *
   * Private files are only accessible if the user
   * has the permission to do so. The permission is most times
   * coupled to the access permission to the parent entity.
   * E.g. if the media entity is unpublished, a user can only access
   * the file, if permission to view the unpublished media entity is granted.
   *
   * @param \Drupal\file\Entity\File $file
   *   The file.
   * @param \Drupal\media\MediaInterface $entity
   *   The current media entity.
   */
  private function moveFileToPrivateStorage(File $file, MediaInterface $entity): void {
    $uri = $file->getFileUri();

    // Ignore generic thumbnails.
    if (strpos($uri, 'generic/generic.png') !== FALSE) {
      return;
    }

    $new_uri = $this->changeFilePathToPrivate($uri);
    $directory = $this->fileSystem->dirname($new_uri);

    // Check if the new directory exists.
    if (!file_exists($directory)) {
      $this->fileSystem->mkdir($directory, NULL, TRUE);
    }

    // Move file. This will also delete image styles.
    $this->fileRepository->move($file, $new_uri);

    $this->logger->info(
      'Moved file "@fileName" (@filedId) of Media Entity @mediaId to public storage.',
      [
        '@fileName' => $file->getFilename(),
        '@filedId' => $file->id(),
        '@mediaId' => $entity->id(),
      ],
    );
  }

  /**
   * Move all files of an entity to private storage.
   *
   * E.g. when a media entity is unpublished.
   *
   * @param \Drupal\media\MediaInterface $entity
   *   The current entity.
   */
  public function moveAllFilesToPrivate(MediaInterface $entity): void {
    $files_to_move = $this->getAllFiles($entity);

    /** @var \Drupal\file\Entity\File[] $files */
    $files = $this->fileStorage->loadMultiple($files_to_move);
    foreach ($files as $file) {
      $this->moveFileToPrivateStorage($file, $entity);
    }
  }

  /**
   * Removes all files used by an entity.
   *
   * E.g. when a media entity is deleted.
   *
   * @param \Drupal\media\MediaInterface $entity
   *   The current entity.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function removeAllFilesOfEntity(MediaInterface $entity): void {
    $files_to_delete = $this->getAllFiles($entity);

    /** @var \Drupal\file\Entity\File[] $files */
    $files = $this->fileStorage->loadMultiple($files_to_delete);
    foreach ($files as $file) {
      $this->deleteFile($file, $entity);
    }
  }

  /**
   * Move the files used by the default revision to public storage.
   *
   * E.g. when a media entity is published.
   *
   * @param \Drupal\media\MediaInterface $entity
   *   The current entity.
   */
  public function moveDefaultRevisionFilesToPublic(MediaInterface $entity): void {
    $fields = $this->getAllFieldsWithFiles($entity);

    // Check if fields setting is public, otherwise skip the field.
    // We do not want to accidentally allow access to all files which
    // should be checked by access rules.
    foreach ($fields as $field) {
      $item_definition = $field->getItemDefinition();
      if ($item_definition->getSetting('uri_scheme') !== 'public') {
        unset($field, $fields);
      }
    }

    // Proceed if there are any fields with file storage left.
    if (isset($fields) && is_array($fields)) {
      $files_to_move = $this->getAllFilesPerLanguage($entity, $fields);

      // Move the files to public.
      $files = $this->fileStorage->loadMultiple($files_to_move);
      foreach ($files as $file) {
        $uri = $file->getFileUri();

        // If the file is already public, do nothing.
        if (strpos($uri, 'public://') === 0) {
          continue;
        }

        $new_uri = $this->changeFilePathToPublic($uri);
        $directory = $this->fileSystem->dirname($new_uri);

        // Check if the new directory exists.
        if (!file_exists($directory)) {
          $this->fileSystem->mkdir($directory, NULL, TRUE);
        }

        // Move file.
        $this->fileRepository->move($file, $new_uri);

        $this->logger->info(
          'Moved file "@fileName" (@filedId) of Media Entity @mediaId to public storage.',
          [
            '@fileName' => $file->getFilename(),
            '@filedId' => $file->id(),
            '@mediaId' => $entity->id(),
          ],
        );
      }
    }
  }

  /**
   * Handle the replacement of files in an entity.
   *
   * @param \Drupal\media\MediaInterface $entity
   *   The current entity.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function handleNewFiles(MediaInterface $entity): void {
    // Check if the entity has a new file.
    // If true, delete the old one, if not used by
    // any other language or revision.
    $fields = $this->getAllFieldsWithFiles($entity);
    $files_of_all_revisions = $this->getAllFilesFromAllRevisionsFromEntity($entity);
    $files_of_current_revision = $this->getAllFilesPerLanguage($entity, $fields);

    // Get files from old (=original) entity.
    $old_files = [];
    foreach ($fields as $field_name => $field) {
      $original_entity = $entity->original;
      if ($original_entity !== NULL && $original_entity->hasField($field_name)) {
        $values = $original_entity->get($field_name)->getValue();
        foreach ($values as $value) {
          $old_files[$value['target_id']] = $value['target_id'];
        }
      }
    }

    // Check if old files are used somewhere else. If not, delete.
    foreach ($old_files as $old_file) {
      // Case 1: File is not used anywhere, delete it.
      if (!in_array($old_file, $files_of_all_revisions, TRUE)) {
        $file = $this->fileStorage->load($old_file);
        if ($file !== NULL) {
          $this->deleteFile($file, $entity);
        }
      }
      // Case 2: The current revision is using the file.
      elseif (!in_array($old_file, $files_of_current_revision, TRUE)) {
        $file = $this->fileStorage->load($old_file);
        if ($file !== NULL) {
          if ($entity->isNewRevision()) {
            // Case 2.1: A new revisions is being created,
            // so we keep the file in private storage.
            $this->moveFileToPrivateStorage($file, $entity);
          }
          else {
            // Case 2.2: The current revision is overwritten,
            // so the file will be not in use afterwards.
            $this->deleteFile($file, $entity);
          }
        }
      }
    }
  }

  /**
   * Remove files of specific revision.
   *
   * @param \Drupal\media\MediaInterface $entity
   *   The current entity.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function removeFilesOfEntityRevision(MediaInterface $entity): void {
    $fields = $this->getAllFieldsWithFiles($entity);
    $files_of_all_revisions = $this->getAllFilesFromAllRevisionsFromEntity($entity);

    $files_from_revision = [];
    foreach ($fields as $field_name => $field) {
      if ($entity->hasField($field_name)) {
        $values = $entity->get($field_name)->getValue();
        foreach ($values as $value) {
          $files_from_revision[$value['target_id']] = $value['target_id'];
        }
      }
    }

    // Check if old revision files are used somewhere else. If not, delete.
    foreach ($files_from_revision as $revision_file) {
      if (!in_array($revision_file, $files_of_all_revisions, TRUE)) {
        $revision_file = $this->fileStorage->load($revision_file);
        if ($revision_file !== NULL) {
          $this->deleteFile($revision_file, $entity);
        }
      }
    }
  }

  /**
   * Remove files of specific revision.
   *
   * @param \Drupal\media\MediaInterface $entity
   *   The current entity.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function removeAllFilesOfTranslation(MediaInterface $entity): void {
    $current_translation_to_delete = $entity->language()->getId();
    $fields = $this->getAllFieldsWithFiles($entity);

    // All files except of the current translation.
    $all_files_of_entity = $this->getAllFiles($entity, [$current_translation_to_delete]);

    // Get all files used by current translation.
    $files_of_current_translation = [];
    foreach ($fields as $field_name => $field) {
      if ($entity->hasField($field_name)) {
        $values = $entity->get($field_name)->getValue();
        foreach ($values as $value) {
          $files_of_current_translation[$value['target_id']] = $value['target_id'];
        }
      }
    }

    // Check if translated files are used somewhere else. If not, delete.
    foreach ($files_of_current_translation as $translated_file) {
      if (!in_array($translated_file, $all_files_of_entity, TRUE)) {
        $translated_file = $this->fileStorage->load($translated_file);
        if ($translated_file !== NULL) {
          $this->deleteFile($translated_file, $entity);
        }
      }
    }
  }

  /**
   * Get all files used by Media entity.
   *
   * @param \Drupal\media\MediaInterface $entity
   *   The Media Entity.
   * @param array $excluded_languages
   *   Languages to skip deletion.
   *
   * @return array
   *   All files.
   */
  private function getAllFiles(MediaInterface $entity, array $excluded_languages = []): array {
    $all_files = [];

    // Get files from default revision.
    array_push($all_files, ...$this->getAllFilesFromDefaultRevisionFromEntity($entity, $excluded_languages));

    // Get files from non default revision and translations.
    array_push($all_files, ...$this->getAllFilesFromAllRevisionsFromEntity($entity, $excluded_languages));

    return $all_files;
  }

}
