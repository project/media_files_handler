<?php

namespace Drupal\Tests\media_files_handler\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests module dependencies are enabled.
 */
class ModuleDependenciesTest extends BrowserTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = [
    'media_files_handler',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Test that media and file modules are enabled as a dependency.
   */
  public function testMediaModuleEnabled(): void {
    $this->assertTrue(\Drupal::moduleHandler()->moduleExists('file'));
    $this->assertTrue(\Drupal::moduleHandler()->moduleExists('media'));
  }

}
