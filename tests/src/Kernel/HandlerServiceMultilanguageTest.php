<?php

namespace Drupal\Tests\media_files_handler\Kernel;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\media\Entity\Media;
use Drupal\media\MediaTypeInterface;
use Drupal\Tests\media\Kernel\MediaKernelTestBase;

/**
 * Tests for the File access control.
 *
 * @group file
 */
class HandlerServiceMultilanguageTest extends MediaKernelTestBase {

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The auto created media type.
   *
   * @var \Drupal\media\MediaTypeInterface
   */
  private $mediaType;

  /**
   * The auto created field name.
   *
   * @var string
   */
  private $sourceFieldName;

  /**
   * The Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The media storage.
   *
   * @var \Drupal\Core\Entity\ContentEntityStorageInterface
   */
  protected $mediaStorage;

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = [
    'media_files_handler',
    'media',
    'media_test_source',
    'image',
    'user',
    'field',
    'system',
    'file',
    'language',
    'content_translation',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['language']);

    $this->entityTypeManager = $this->container->get('entity_type.manager');
    $this->mediaStorage = $this->entityTypeManager->getStorage('media');
    $this->setSetting('file_private_path', $this->container->getParameter('site.path') . '/private');
    $this->state = $this->container->get('state');

    // Create second language 'de'.
    $this->container->get('config.installer')->installDefaultConfig('module', 'language');
    $language = ConfigurableLanguage::create([
      'id' => 'de',
      'label' => 'German',
      'weight' => 1,
    ]);
    $language->save();

    $this->mediaType = $this->createMediaType('file');
    $this->container->get('content_translation.manager')
      ->setEnabled('media', $this->mediaType->id(), TRUE);

    $this->sourceFieldName = $this->mediaType->getSource()
      ->getSourceFieldDefinition($this->mediaType)
      ->getName();

    // Enable translations for the media entity type.
    $this->state->set('media.translation', TRUE);
    $this->state->set('media.field_definitions.translatable', [
      'label' => TRUE,
      $this->sourceFieldName => TRUE,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container): void {
    parent::register($container);

    $container->register('stream_wrapper.public', 'Drupal\Core\StreamWrapper\PublicStream')
      ->addTag('stream_wrapper', ['scheme' => 'public']);

    $container->register('stream_wrapper.private', 'Drupal\Core\StreamWrapper\PrivateStream')
      ->addTag('stream_wrapper', ['scheme' => 'private']);
  }

  /**
   * Helper to generate a media item.
   *
   * @param string $filename
   *   String filename with extension.
   * @param \Drupal\media\MediaTypeInterface $media_type
   *   The media type.
   * @param bool $status
   *   The status of the created entity.
   *
   * @return \Drupal\media\Entity\Media
   *   A media item.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function generateMedia($filename, MediaTypeInterface $media_type, $status = TRUE): Media {
    file_put_contents('public://' . $filename, NULL);
    $this->assertFileExists('public://' . $filename);
    $file = File::create([
      'uri' => 'public://' . $filename,
      'uid' => $this->user->id(),
    ]);
    $file->setPermanent();
    $file->save();

    return Media::create([
      'bundle' => $media_type->id(),
      'name' => 'Mr. Jones',
      'field_media_file' => [
        'target_id' => $file->id(),
      ],
      'status' => $status,
      'langcode' => 'en',
    ]);
  }

  /**
   * Test Scenario 8, 10.
   *
   * Tests a media file is set to temporary when media entity is deleted.
   */
  public function testSetFileToTemporaryWhenDeletingMedia(): void {
    $source = $this->sourceFieldName;

    // Create Media Entity.
    $media = $this->generateMedia('text.txt', $this->mediaType);
    $media->save();
    $originalFileID = $this->getFile($media)->id();

    // Create new file.
    file_put_contents('public://text2.txt', NULL);
    $this->assertFileExists('public://text2.txt');
    $newFile = File::create([
      'uri' => 'public://text2.txt',
      'uid' => $this->user->id(),
    ]);
    $newFile->setPermanent();
    $newFile->save();

    // Cache File ID.
    $newFileID = $newFile->id();

    // Add translation to media entity and set new file.
    $media = $media->addTranslation('de');
    $media->setName('DE');
    $media->set($source, ['target_id' => $newFile->id()]);
    $media->save();

    // Validate original file is still in use.
    $file1 = File::load($originalFileID);
    $this->assertTrue($file1->isPermanent());
    $usage1 = \Drupal::service('file.usage')->listUsage($file1);
    $this->assertNotEmpty($usage1);
    $this->assertFileExists('public://text.txt');

    // Validate new file is in use.
    $translatedFile = File::load($newFileID);
    $this->assertTrue($translatedFile->isPermanent());
    $usageTranslatedFile = \Drupal::service('file.usage')->listUsage($translatedFile);
    $this->assertNotEmpty($usageTranslatedFile);
    $this->assertFileExists('public://text2.txt');

    // Delete Media entity.
    $media->delete();

    // New File should be set to temporary.
    $translatedFile = File::load($newFileID);
    $this->assertTrue($translatedFile->isTemporary());
    // File usage should be empty.
    $usageTranslatedFile = \Drupal::service('file.usage')->listUsage($translatedFile);
    $this->assertEmpty($usageTranslatedFile);

    // Original File should be set to temporary.
    $original_file = File::load($originalFileID);
    $this->assertTrue($original_file->isTemporary());
    // File usage should be empty.
    $usageOriginalFile = \Drupal::service('file.usage')->listUsage($original_file);
    $this->assertEmpty($usageOriginalFile);
  }

  /**
   * Test Scenario 9.
   *
   * Tests a media file is set to temporary
   * whe na media translation is deleted.
   */
  public function testSetFileToTemporaryWhenDeletingTranslationOfMedia(): void {
    $source = $this->sourceFieldName;

    // Create Media Entity.
    $media = $this->generateMedia('text.txt', $this->mediaType);
    $media->save();
    $originalFileID = $this->getFile($media)->id();

    // Create new file.
    file_put_contents('public://text2.txt', NULL);
    $this->assertFileExists('public://text2.txt');
    $newFile = File::create([
      'uri' => 'public://text2.txt',
      'uid' => $this->user->id(),
    ]);
    $newFile->setPermanent();
    $newFile->save();

    // Cache File ID.
    $newFileID = $newFile->id();

    // Add translation to media entity and set new file.
    $media = $media->addTranslation('de');
    $media->setName('DE');
    $media->set($source, ['target_id' => $newFile->id()]);
    $media->save();

    // Validate original file is still in use.
    $file1 = File::load($originalFileID);
    $this->assertTrue($file1->isPermanent());
    $usage1 = \Drupal::service('file.usage')->listUsage($file1);
    $this->assertNotEmpty($usage1);
    $this->assertFileExists('public://text.txt');

    // Validate new file is in use.
    $translatedFile = File::load($newFileID);
    $this->assertTrue($translatedFile->isPermanent());
    $usageTranslatedFile = \Drupal::service('file.usage')->listUsage($translatedFile);
    $this->assertNotEmpty($usageTranslatedFile);
    $this->assertFileExists('public://text2.txt');

    // Delete translation of Media entity.
    $untranslated_entity = $media->getUntranslated();
    $untranslated_entity->removeTranslation($media->language()->getId());
    $untranslated_entity->save();

    // New File should be set to temporary.
    $translatedFile = File::load($newFileID);
    $this->assertTrue($translatedFile->isTemporary());
    // File usage should be empty.
    $usageTranslatedFile = \Drupal::service('file.usage')->listUsage($translatedFile);
    $this->assertEmpty($usageTranslatedFile);

    // Original File should still be permanent.
    $original_file = File::load($originalFileID);
    $this->assertTrue($original_file->isPermanent());
    // File usage should not be empty.
    $usageOriginalFile = \Drupal::service('file.usage')->listUsage($original_file);
    $this->assertNotEmpty($usageOriginalFile);
  }

  /**
   * Get specific file of media entity.
   *
   * @param \Drupal\media\Entity\Media $media
   *   The Media entity.
   *
   * @return \Drupal\file\FileInterface
   *   The file.
   */
  private function getFile(Media $media): FileInterface {
    $source_field_name = $this->mediaType->getSource()
      ->getSourceFieldDefinition($this->mediaType)
      ->getName();

    return $media->get($source_field_name)->entity;
  }

}
