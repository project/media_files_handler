<?php

namespace Drupal\Tests\media_files_handler\Kernel;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;
use Drupal\media\Entity\Media;
use Drupal\media\MediaTypeInterface;
use Drupal\Tests\media\Kernel\MediaKernelTestBase;

/**
 * Tests for the File access control.
 *
 * @group file
 */
class HandlerServiceTest extends MediaKernelTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = [
    'media_files_handler',
    'media',
    'media_test_source',
    'image',
    'user',
    'field',
    'system',
    'file',
  ];

  /**
   * The Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The media storage.
   *
   * @var \Drupal\Core\Entity\ContentEntityStorageInterface
   */
  protected $mediaStorage;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->entityTypeManager = $this->container->get('entity_type.manager');
    $this->mediaStorage = $this->entityTypeManager->getStorage('media');
    $this->setSetting('file_private_path', $this->container->getParameter('site.path') . '/private');
  }

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    parent::register($container);

    $container->register('stream_wrapper.public', 'Drupal\Core\StreamWrapper\PublicStream')
      ->addTag('stream_wrapper', ['scheme' => 'public']);

    $container->register('stream_wrapper.private', 'Drupal\Core\StreamWrapper\PrivateStream')
      ->addTag('stream_wrapper', ['scheme' => 'private']);
  }

  /**
   * Helper to generate a media item.
   *
   * @param string $filename
   *   String filename with extension.
   * @param \Drupal\media\MediaTypeInterface $media_type
   *   The media type.
   * @param bool $status
   *   The status of created entity.
   *
   * @return \Drupal\media\Entity\Media
   *   A media item.
   */
  protected function generateMedia($filename, MediaTypeInterface $media_type, $status = TRUE): Media {
    file_put_contents('public://' . $filename, NULL);
    $this->assertFileExists('public://' . $filename);
    $file = File::create([
      'uri' => 'public://' . $filename,
      'uid' => $this->user->id(),
    ]);
    $file->setPermanent();
    $file->save();

    return Media::create([
      'bundle' => $media_type->id(),
      'name' => 'Mr. Jones',
      'field_media_file' => [
        'target_id' => $file->id(),
      ],
      'status' => $status,
    ]);
  }

  /**
   * Test Scenario 1.
   *
   * Tests a media file is set to temporary
   * when media entity is deleted.
   */
  public function testSetFileToTemporaryWhenDeletingMedia(): void {
    // Create Media Entity.
    $mediaType = $this->createMediaType('file');
    $media = $this->generateMedia('text.txt', $mediaType);
    $media->save();

    // Delete Media entity.
    $media->delete();

    $file = $this->getFile($mediaType, $media);

    // File should be set to temporary.
    $this->assertTrue($file->isTemporary());

    // File usage should be empty.
    $usage = \Drupal::service('file.usage')->listUsage($file);
    $this->assertEmpty($usage);
  }

  /**
   * Test Scenario 2.
   *
   * Tests a media file is set to temporary when media entity
   * gets a new file without creating a new revision of media.
   */
  public function testSetFileToTemporaryWhenReplacingFileWithoutNewRevisionOfMedia(): void {
    // Create Media Entity.
    $mediaType = $this->createMediaType('file');
    $media = $this->generateMedia('text.txt', $mediaType);
    $media->save();

    // Create new file.
    file_put_contents('public://text2.txt', NULL);
    $this->assertFileExists('public://text2.txt');
    $newFile = File::create([
      'uri' => 'public://text2.txt',
      'uid' => $this->user->id(),
    ]);
    $newFile->setPermanent();
    $newFile->save();

    // Update Media entity with new file.
    $source_field_name = $mediaType->getSource()
      ->getSourceFieldDefinition($mediaType)
      ->getName();
    $media->set($source_field_name, ['target_id' => $newFile->id()]);
    $media->save();

    // Cache original file.
    $file = File::load(1);
    // File should be set to temporary.
    $this->assertTrue($file->isTemporary());
    // File usage should be empty.
    $usage = \Drupal::service('file.usage')->listUsage($file);
    $this->assertEmpty($usage);
  }

  /**
   * Test Scenario 3.
   *
   * Tests a media file is moved to private storage when
   * media entity gets a new file while creating a new revision of media.
   */
  public function testSetFileToTemporaryWhenReplacingFileWithCreatingNewRevisionOfMedia() {
    // Create Media Entity.
    $mediaType = $this->createMediaType('file');
    $media = $this->generateMedia('text.txt', $mediaType);
    $media->save();

    // Create new file.
    file_put_contents('public://text2.txt', NULL);
    $this->assertFileExists('public://text2.txt');
    $newFile = File::create([
      'uri' => 'public://text2.txt',
      'uid' => $this->user->id(),
    ]);
    $newFile->setPermanent();
    $newFile->save();

    // Update Media entity with new file.
    $source_field_name = $mediaType->getSource()
      ->getSourceFieldDefinition($mediaType)
      ->getName();
    $media->set($source_field_name, ['target_id' => $newFile->id()]);
    $media->setNewRevision(TRUE);
    $media->save();

    // Cache original file.
    $file = File::load(1);
    // File should still be permanent.
    $this->assertTrue($file->isPermanent());

    // File should have moved to private.
    $this->assertFileExists('private://text.txt');
    $this->assertFileDoesNotExist('public://text.txt');
  }

  /**
   * Test Scenario 4.
   *
   * Tests a media file is set to temporary when its non
   * default media revision entity is deleted.
   */
  public function testSetFileToTemporaryWhenDeletingNonDefaultRevisionOfMedia(): void {
    // Create Media Entity.
    $mediaType = $this->createMediaType('file');
    $media = $this->generateMedia('text.txt', $mediaType);
    $media->save();

    // Create new file.
    file_put_contents('public://text2.txt', NULL);
    $this->assertFileExists('public://text2.txt');
    $newFile = File::create([
      'uri' => 'public://text2.txt',
      'uid' => $this->user->id(),
    ]);
    $newFile->setPermanent();
    $newFile->save();

    // Update Media entity with new file and new revision.
    $source_field_name = $mediaType->getSource()
      ->getSourceFieldDefinition($mediaType)
      ->getName();
    $media->set($source_field_name, ['target_id' => $newFile->id()]);
    $media->setNewRevision(TRUE);
    $media->save();

    // Delete initial revision of media entity.
    $this->mediaStorage->deleteRevision(1);

    // Cache original file.
    $file = File::load(1);
    // File should be set to temporary.
    $this->assertTrue($file->isTemporary());
    // File usage should be empty.
    $usage = \Drupal::service('file.usage')->listUsage($file);
    $this->assertEmpty($usage);
  }

  /**
   * Test Scenario 5.
   *
   * Tests both media files are set to temporary when
   * a media entity with two revisions is deleted.
   */
  public function testSetFilesToTemporaryWhenDeletingMediaWithTwoRevisions(): void {
    // Create Media Entity.
    $mediaType = $this->createMediaType('file');
    $media = $this->generateMedia('text.txt', $mediaType);
    $media->save();
    $originalFileId = $media->id();

    // Create new file.
    file_put_contents('public://text2.txt', NULL);
    $this->assertFileExists('public://text2.txt');
    $newFile = File::create([
      'uri' => 'public://text2.txt',
      'uid' => $this->user->id(),
    ]);
    $newFile->setPermanent();
    $newFile->save();
    $newFileId = $newFile->id();

    // Update Media entity with new file and new revision.
    $source_field_name = $mediaType->getSource()
      ->getSourceFieldDefinition($mediaType)
      ->getName();
    $media->set($source_field_name, ['target_id' => $newFile->id()]);
    $media->setNewRevision(TRUE);
    $media->save();

    // Delete media entity.
    $media->delete();

    // Cache original file.
    $file1 = File::load($originalFileId);
    // File should be set to temporary.
    $this->assertTrue($file1->isTemporary());
    // File usage should be empty.
    $usage1 = \Drupal::service('file.usage')->listUsage($file1);
    $this->assertEmpty($usage1);

    // Cache original file.
    $file2 = File::load($newFileId);
    // File should be set to temporary.
    $this->assertTrue($file2->isTemporary());
    // File usage should be empty.
    $usage2 = \Drupal::service('file.usage')->listUsage($file2);
    $this->assertEmpty($usage2);
  }

  /**
   * Test Scenario 6.
   *
   * Tests a media file is moved to private storage
   * when media entity is set to unpublished.
   */
  public function testSetFileToPrivateWhenUnpublishingMedia() {
    // Create Media Entity.
    $filename = 'text.txt';
    $mediaType = $this->createMediaType('file');
    $media = $this->generateMedia($filename, $mediaType);
    $media->save();

    $file = $this->getFile($mediaType, $media);
    $fileUri = $file->getFileUri();
    $this->assertStringContainsString('public', $fileUri);

    // Set Media Entity status to unpublished.
    $media->setUnpublished();
    $media->save();

    $this->assertFileExists('private://' . $filename);
    $this->assertFileDoesNotExist('public://' . $filename);
  }

  /**
   * Test Scenario 7.
   *
   * Tests a media file is moved to public storage
   * when media entity is set to published.
   */
  public function testSetFileToPublicWhenPublishingMedia() {
    // Create Media Entity.
    $filename = 'text.txt';
    $mediaType = $this->createMediaType('file');
    $media = $this->generateMedia($filename, $mediaType, FALSE);
    $media->setUnpublished();
    $media->save();

    // Make sure the file is initially stored as private.
    $this->assertFileExists('private://' . $filename);
    $this->assertFileDoesNotExist('public://' . $filename);

    // Set Media Entity status to published.
    $media->setPublished();
    $media->save();

    // The file should no be public.
    $this->assertFileExists('public://' . $filename);
    $this->assertFileDoesNotExist('private://' . $filename);
  }

  /**
   * Get specific file of media entity.
   *
   * @param \Drupal\media\MediaTypeInterface $mediaType
   *   The bundle.
   * @param \Drupal\media\Entity\Media $media
   *   The Media entity.
   *
   * @return \Drupal\file\FileInterface
   *   The file.
   */
  private function getFile(MediaTypeInterface $mediaType, Media $media): FileInterface {
    $source_field_name = $mediaType->getSource()
      ->getSourceFieldDefinition($mediaType)
      ->getName();

    return $media->get($source_field_name)->entity;
  }

}
