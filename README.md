# Media Files Handler

A common problem when using the Media entity as media library are lingering
files after deleting a Media entity or replacing a file in a Media entity. The
result is that those orphaned files are still downloadable from the server. E.G.
if a link to file has been indexed by Google, the file could still be found via a
Google search, while the website editor expects the files to be gone, or
unreachable at least.

This module tries to find ways
around [#3027324: When Media entities are deleted, the associated files are not deleted](https://www.drupal.org/project/drupal/issues/3027324)
and [#2821423: Dealing with unexpected file deletion due to incorrect file usage](https://www.drupal.org/project/drupal/issues/2821423)
.

On each update of a media item this module checks if all files of this media
entity are still used in any translation or revision.

## Requirements

Drupal 9, Media Entity

## Features

* Files still used by an older revision are moved into private storage, because
  the file should not be public accessible anymore.
* If the file field of the media entity is translatable, the files of
  translations of the current revision should not be touched, if the translation
  is published.
* If the file is not used in any revision or translation, set the file to
  temporary or delete it.
* If an published media entity get unpublished, move all files to private
  storage.
* If a media entity gets published, move the files of the current revision to
  public storage, if this is the original field definition.

Also see test
coverage: https://www.drupal.org/project/media_files_handler/issues/3273297

Each time a file is touched, a log is written to the Drupal log.

## Installation

There is currently no UI for settings. Just enable and install the module as
usual.

If you use revisions or the unpublished state for media, make sure the private
file storage is configured. There are currently no checks for
this: [https://www.drupal.org/project/media_files_handler/issues/3280876](https://www.drupal.org/project/media_files_handler/issues/3280876)

## Road Map

Current Ideas:

* [Add replace option](https://www.drupal.org/project/media_files_handler/issues/3233131)
* [Expose Option to Delete or Set File to Temporary in UI](https://www.drupal.org/project/media_files_handler/issues/3278426)
* [Add Service to find and delete orphaned files of media elements](https://www.drupal.org/project/media_files_handler/issues/3279208)
